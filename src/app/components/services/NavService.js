(function(){
  'use strict';

  angular.module('app')
          .service('navService', [
          '$q',
          navService
  ]);

  function navService($q){
    var menuItems = [
      {
        name: 'Dashboard',
        icon: 'dashboard',
        sref: '.dashboard'
      },
      {
        name: 'Empresas',
        icon: 'domain',
        sref: '.companies'
      },
      {
        name: 'Clientes',
        icon: 'people',
        sref: '.clients'
      },
      {
        name: 'Perfis',
        icon: 'security',
        sref: '.profiles'
      },
      {
        name: '',
        icon: 'power_settings_new',
        sref: '.login'
      }
    ];

    return {
      loadAllItems : function() {
        return $q.when(menuItems);
      }
    };
  }

})();
