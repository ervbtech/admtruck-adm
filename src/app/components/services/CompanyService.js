(function(){
  'use strict';

  angular.module('app')
        .service('companyService', [
        '$q', '$http',
      companyService
  ]);

  function companyService($q, $http){

    return {
      loadAllItems : function() {
        var companies = $http.get('http://localhost:3000/api/company')

        return companies;
      },
      save : function(company) {
        if(company._id) {
          return $http.put('http://localhost:3000/api/company/' + company._id, company);
        } else {
          return $http.post('http://localhost:3000/api/company', company);
        }
      }
    };
  }
})();
