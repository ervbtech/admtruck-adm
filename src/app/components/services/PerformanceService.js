(function () {
    'use strict';

    angular.module('app')
        .service('performanceService', [
            '$http', '$q',
            performanceService
        ]);

    function performanceService($http, $q) {

        var service = {
            getPerformanceData: getPerformanceData,
            dashboard : function() {
              return new Promise(function(resolve, reject) {
                $http.get('http://localhost:3000/api/users')
                .then(function(resp) {
                   var clients = resp.data;
                   var activeCount = 0, inactiveCount = 0;
                   for (var i = 0; i < clients.length; i++) {
                     var client = clients[i]
                     activeCount += client.active ? 1 : 0;
                     inactiveCount += !client.active ? 1 : 0;
                   }
                   resolve({
                     total: clients.length,
                     activeCount: activeCount,
                     inactiveCount: inactiveCount
                   });
                })
              });
            }
        };

        return service;

        function getPerformanceData(performancePeriod) {
            if (performancePeriod === 'week') {
                return [
                    {
                        "key": 'Ativos',
                        "values": [[1, 11], [2, 10], [3, 14], [4, 21], [5, 13], [6, 21], [7, 21], [8, 18], [9, 11], [10, 11], [11, 18], [12, 14]]
                    },

                    {
                        "key": 'Inativos',
                        "values": [[1, 29], [2, 36], [3, 42], [4, 25], [5, 22], [6, 34], [7, 41], [8, 19], [9, 45], [10, 31], [11, 28], [12, 36]]
                    }
                ]
            } else {
                return [
                    {
                        "key": 'Ativos',
                        "values": [[1, 13], [2, 14], [3, 24], [4, 18], [5, 16], [6, 14], [7, 11], [8, 13], [9, 15], [10, 11], [11, 18], [12, 16]]
                    },

                    {
                        "key": 'Inativos',
                        "values": [[1, 29], [2, 36], [3, 42], [4, 25], [5, 22], [6, 34], [7, 41], [8, 19], [9, 45], [10, 31], [11, 28], [12, 36]]
                    }
                ]
            }
        }
    }
})();
