(function(){
  'use strict';

  angular.module('app')
        .service('clientService', [
        '$q', '$http',
      clientService
  ]);

  function clientService($q, $http){

    return {
      loadAllItems : function() {
        var clients = $http.get('http://localhost:3000/api/users')

        return clients;
      },
      save : function(client) {
        if(client._id) {
          return $http.put('http://localhost:3000/api/users/' + client._id, client);
        } else {
          client.password = '123456';
          return $http.post('http://localhost:3000/api/users', client);
        }
      },
      delete: function(client) {
        return $http.delete('http://localhost:3000/api/users/'+client.email)
      }
    };
  }
})();
