(function () {
  angular
  .module('app')
  .controller('MemoryController', [
    'performanceService',
    MemoryController
  ]);

  function MemoryController(performanceService) {
    var vm = this;
    vm.totalClients = 0;
    vm.chart = {};
    vm.chart.data = [];
    vm.chart.options = {
      chart: {
        type: 'pieChart',
        height: 210,
        donut: true,
        pie: {
          startAngle: function (d) { return d.startAngle/2 -Math.PI/2 },
          endAngle: function (d) { return d.endAngle/2 -Math.PI/2 }
        },
        x: function (d) { return d.key; },
        y: function (d) { return d.y; },
        valueFormat: (d3.format(".0f")),
        color: ['rgb(0, 150, 136)', 'rgb(231, 87, 83)'],
        showLabels: false,
        showLegend: true,
        tooltips: false,
        title: vm.chart.data.length ? parseFloat((vm.chart.data[0].y / vm.totalClients) * 100).toFixed(2)  + '%' : '',
        titleOffset: -10,
        margin: { bottom: -80, left: -20, right: -20 }
      }
    };

    performanceService.dashboard()
    .then(function(data) {
      vm.totalClients = data.total;
      vm.chart.data = [ {key: 'Utilizando', y: data.activeCount}, { key: 'Descontinuados', y: data.inactiveCount} ];
      vm.chart.api.refresh();
    });
  }
})();
