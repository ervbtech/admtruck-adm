(function(){

  angular
  .module('app')
  .controller('CompanyController', [
    'companyService',
    '$scope',
    '$mdToast',
    '$mdEditDialog',
    CompanyController

  ]);

  function CompanyController(companyService , $scope, $mdToast, $mdEditDialog) {
    var vm = this;

    vm.tableData = [];
    vm.totalItems = 0;

    $scope.query = {
      order: 'name',
      limit: 10,
      page: 1
    };
    vm.selected = null;
    vm.new = null;

    vm.getItems = function() {
      GetItemsData($scope.query);
    }

    function GetItemsData(query) {
      companyService
      .loadAllItems()
      .then(function(response) {
        vm.tableData =  response.data;
        // Represents the count of database count of records, not items array!
        vm.totalItems = vm.tableData.length;

      });

    }

    GetItemsData($scope.query);

    $scope.save = function(company) {
      companyService
      .save(company)
      .then(function(response) {
        vm.getItems();
        if(company._id == vm.selected._id) {
          vm.selected = null;
        } else if(company._id == null) {
          $scope.new = null;
        }
        vm.showSimpleToast('Registro salvo com sucesso.');
      });
    }

    $scope.selectItem = function(item)  {
      console.log(item);
      vm.selected = item;
    };

    vm.showSimpleToast = function(msg) {
      $mdToast.show(
        $mdToast.simple()
        .content(msg)
        .hideDelay(2000)
        .position('bottom right')
      );
    }

    $scope.cancel = function(evt) {
      vm.selected = {}
      evt.stopPropagation()
    }

    $scope.edit = function (event, item, prop) {
      // if auto selection is enabled you will want to stop the event
      // from propagating and selecting the row
      event.stopPropagation();

      /*
      * messages is commented out because there is a bug currently
      * with ngRepeat and ngMessages were the messages are always
      * displayed even if the error property on the ngModelController
      * is not set, I've included it anyway so you get the idea
      */

      var promise = $mdEditDialog.small({
        // messages: {
        //   test: 'I don\'t like tests!'
        // },
        modelValue: item[prop],
        placeholder: 'Edite aqui',
        save: function (input) {
          item[prop] = input.$modelValue;
          vm.selected = item;
        },
        targetEvent: event,
        validators: {
          'md-maxlength': 30
        }
      });

      promise.then(function (ctrl) {
        var input = ctrl.getInput();

        input.$viewChangeListeners.push(function () {
          input.$setValidity('test', input.$modelValue !== 'test');
        });
      });
    };
  }


})();
