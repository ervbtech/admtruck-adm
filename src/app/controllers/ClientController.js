(function(){

  angular
  .module('app')
  .controller('ClientController', [
    'clientService',
    '$scope',
    '$mdToast',
    '$mdEditDialog',
    '$mdDialog',
    ClientController

  ]);

  function ClientController(clientService , $scope, $mdToast, $mdEditDialog, $mdDialog) {
    var vm = this;

    vm.tableData = [];
    vm.totalItems = 0;

    $scope.query = {
      order: 'name',
      limit: 10,
      page: 1
    };

    vm.selected = null;
    vm.new = null;

    function GetItemsData(query) {
      clientService
      .loadAllItems()
      .then(function(response) {
        vm.tableData =  response.data;
        // Represents the count of database count of records, not items array!
        vm.totalItems = vm.tableData.length;

      });

    }

    GetItemsData($scope.query);

    $scope.save = function(evt, client) {
      clientService
      .save(client)
      .then(function(response) {
        vm.showSimpleToast('Registro salvo com sucesso.');
        GetItemsData($scope.query);
      });
    }

    $scope.cancel = function(evt) {
      vm.selected = {}
    }

    $scope.delete = function(client) {
      clientService
      .delete(client)
      .then(function(response) {
        vm.showSimpleToast('Registro removido com sucesso.');
        GetItemsData($scope.query);
      });
    }

    $scope.edit = function (event, item, prop) {
      // if auto selection is enabled you will want to stop the event
      // from propagating and selecting the row
      event.stopPropagation();

      /*
      * messages is commented out because there is a bug currently
      * with ngRepeat and ngMessages were the messages are always
      * displayed even if the error property on the ngModelController
      * is not set, I've included it anyway so you get the idea
      */

      var promise = $mdEditDialog.small({
        // messages: {
        //   test: 'I don\'t like tests!'
        // },
        modelValue: item[prop],
        placeholder: 'Edite aqui',
        save: function (input) {
          item[prop] = input.$modelValue;
          vm.selected = item;
        },
        targetEvent: event,
        validators: {
          'md-maxlength': 30
        }
      });

      promise.then(function (ctrl) {
        var input = ctrl.getInput();

        input.$viewChangeListeners.push(function () {
          input.$setValidity('test', input.$modelValue !== 'test');
        });
      });
    };

    $scope.showCompaniesDialog = function(ev, client) {
      vm.selected = client;
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'app/views/dialogs/companies-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
      })
      .then(function(company) {
        if(company)
          vm.selected.company = company;
      }, function() {

      });
    };

    function DialogController($scope, $mdDialog) {
      $scope.hide = function() {
        $mdDialog.hide();
      };

      $scope.cancel = function() {
        $mdDialog.cancel();
      };

      $scope.select = function(company) {
        $mdDialog.hide(company);
      };

      $scope.answer = function(answer) {
        $mdDialog.hide(answer);
      };
    }

    vm.showSimpleToast = function(msg) {
      $mdToast.show(
        $mdToast.simple()
        .content(msg)
        .hideDelay(2000)
        .position('bottom right')
      );
    }

    function showSimpleToast(msg) {
      $mdToast.show(
        $mdToast.simple()
        .content(msg)
        .hideDelay(2000)
        .position('bottom right')
      );
    }
  }

})();
